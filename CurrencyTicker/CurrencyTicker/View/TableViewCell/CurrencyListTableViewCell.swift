//
//  CurrencyListTableViewCell.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 18/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import UIKit

class CurrencyListTableViewCell: UITableViewCell {
    @IBOutlet var currencyImageView : UIImageView!
    
    @IBOutlet var currencyNameLabel : UILabel!
    @IBOutlet var currencyCodeLabel : UILabel!
    @IBOutlet var currencySymbolLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureWithCurrency(_ currency : Currency, isFavorite : Bool = false) {
        currencyNameLabel.text = currency.name
        currencyCodeLabel.text = currency.code
        currencySymbolLabel.text = currency.symbol
        
        currencyImageView.image = UIImage(named: currency.code)
        
        if isFavorite {
            accessoryType = .checkmark
        } else {
            accessoryType = .none
        }
    }
}
