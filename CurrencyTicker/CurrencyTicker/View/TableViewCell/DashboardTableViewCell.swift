//
//  DashboardTableViewCell.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 26/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import UIKit

class DashboardTableViewCell: UITableViewCell {
    
    @IBOutlet var currencyImageView : UIImageView!
    
    @IBOutlet var currencyNameLabel : UILabel!
    @IBOutlet var currencyValueLabel : UILabel!
    @IBOutlet var priceChangeContainerView : CurrencyPriceChangeView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureWithExchangeData(_ exchangeData : CurrencyExchange) {
        let currency = exchangeData.currency
        
        currencyImageView.image = UIImage.init(named: currency.code)
        currencyNameLabel.text = currency.code
        currencyValueLabel.text = exchangeData.exchangeRateString
        
        priceChangeContainerView.configureWithExchange(exchangeData)
    }
}
