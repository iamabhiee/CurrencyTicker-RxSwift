//
//  CurrencyPriceChangeView.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 26/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import UIKit

class CurrencyPriceChangeView: UIView {
    
    @IBOutlet var priceChangeValueLabel : UILabel!
    @IBOutlet var priceChangePercentageLabel : UILabel!

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    func configureWithExchange(_ exchangeData : CurrencyExchange) {
        priceChangeValueLabel.text = exchangeData.differenceString
        priceChangePercentageLabel.text = exchangeData.differencePercentageString
        
        backgroundColor = exchangeData.isDifferencePostive ? UIColor.ct_greenColor() : UIColor.ct_redColor()
    }
}
