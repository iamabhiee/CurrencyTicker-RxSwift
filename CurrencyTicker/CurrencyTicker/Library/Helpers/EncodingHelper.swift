//
//  EncodingHelper.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 18/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import Foundation

enum EncodingStructError: Error {
    case invalidSize
}

//func encode<T>(_ value: T) -> Data {
//    var value = value
//    return withUnsafePointer(to: &value) { p in
//        Data(bytes: UnsafePointer<UInt8>(p), count: MemoryLayout.size(ofValue: value))
//    }
//}

func decode<T>(_ data: Data) throws -> T {
    guard data.count == MemoryLayout<T>.size else {
        throw EncodingStructError.invalidSize
    }
    
    let pointer = UnsafeMutablePointer<T>.allocate(capacity: 1)
    (data as NSData).getBytes(pointer, length: data.count)
    
    return pointer.move()
}

enum Result<T> {
    case success(T)
    case failure
}
