//
//  NetworkingHelper.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 30/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import Alamofire

typealias CompletionBlock = (_ result: [String: AnyObject]?, _ error: Error?) -> Void

class NetowrkingHelper {
    let baseURL = "http://api.fixer.io/"
    
    static let sharedInstance = NetowrkingHelper()
    
    init() {
        
    }
    
    func get(_ path : String, parameters : [String: AnyObject]?, completionBlock : @escaping CompletionBlock) {
        print("Get : Path : \(path)")
        
        let finalURLString = baseURL + path
        let finalURL = URL(string: finalURLString)!
        
        let nsurlreq = NSMutableURLRequest(url: finalURL, cachePolicy: NSURLRequest.CachePolicy.returnCacheDataElseLoad, timeoutInterval: 100)
        
        Alamofire.request(finalURL, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: [:])
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    print("Validation Successful")
                    if let JSON = response.result.value as? [String : AnyObject] {
                        print("JSON: \(JSON)")
                        completionBlock(JSON, nil)
                    } else {
                        //JSON Parsing Error
                        let error = NSError(domain: "", code: 0, userInfo: ["" : ""])
                        completionBlock(nil, error)
                    }
                    
                case .failure(let error):
                    print(error)
                    completionBlock(nil, error)
                }
        }
    }
}
