//
//  NSDate+Extension.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 30/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import Foundation

extension Date {
    
    var stringValue : String {
        get {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            return formatter.string(from: self);
        }
    }
    
    var shortStringValue : String {
        get {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd"
            return formatter.string(from: self);
        }
    }
    
    static func dateFromString(_ dateString: String) -> Date! {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.date(from: dateString)
    }
    
    func dateAfterAddingDays(_ days : Double) -> Date {
        return self.addingTimeInterval((days * 60 * 60 * 24))
    }
    
    
}
