//
//  UIViewController+Extension.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 30/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import UIKit
import MBProgressHUD

extension UIViewController {
    func showProgressHud() {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        view.bringSubview(toFront: hud)
    }
    
    func hideProgressHud() {
        MBProgressHUD.hideAllHUDs(for: view, animated: true)
    }
    
    func handleError(_ error : Error?) {
        if let errorValue = error {
            showAlert("Error!", message: errorValue.localizedDescription)
        }
    }
    
    func showAlert(_ title : String, message : String) {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        controller.addAction(okAction)
        
        present(controller, animated: true, completion: nil)
    }
}
