//
//  NSObject+String.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 19/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import Foundation

extension UIView {
    class var nameOfClass: String {
        return NSStringFromClass(self).components(separatedBy: ".").last! as String
    }
    
    //Reuse identifier for UITableViewCell
    class var identifier: String {
        return String(format: "%@_identifier", self.nameOfClass)
    }
}
