//
//  ExchangeDataManager.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 30/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import Foundation

typealias ExchangeCompletionBlock = (_ result: [CurrencyExchange]?, _ error: Error?) -> Void

class ExchangeDataManager: NSObject {
    
    class func fetchLatestExchangeDataWithDifference(_ currencies : [Currency], baseCurrency : Currency, completion : @escaping ExchangeCompletionBlock) {
        let errorCompletionHandler:(Error?)->Void = { error in
            completion(nil, error)
        }
        
        let successCompletionHandler:([CurrencyExchange])->Void = { data in
            completion(data, nil)
        }
        
        let today = Date()
        fetchExchangeData(currencies, baseCurrency: baseCurrency, date: today, success: { data, date in
            if let firstData = data, let firstDate = date {
                let previousDate = firstDate.dateAfterAddingDays(-1)
                
                fetchExchangeData(currencies, baseCurrency: baseCurrency, date: previousDate, success: { data, date in
                    
                    if let secondData = data {
                        for index in 0...firstData.count - 1 {
                            let firstCurrencyExchangeData = firstData[index]
                            let secondCurrencyExchangeData = secondData[index]
                            
                            firstCurrencyExchangeData.previousConversationRate = secondCurrencyExchangeData.conversationRate
                        }
                    }
                    
                    successCompletionHandler(firstData)
                    
                }, failure: errorCompletionHandler)
            } else {
                //TODO : No data error
                errorCompletionHandler(nil)
            }
        }, failure: errorCompletionHandler)
    }
    
    class func fetchPastWeekExchangeData(_ currency : Currency, baseCurrency : Currency, success : @escaping (_ result: [CurrencyExchange]?) -> Void, failure : @escaping (_ error : Error?) -> Void) {
        let date = Date()
        let existingData : [CurrencyExchange] = []
        let days = 7
        
        fetchRecursiveDataForCurrency(currency, baseCurrency: baseCurrency, days: days, date: date, existingData: existingData, success: success, failure: failure)
    }
    
    //MARK : API Calls
    
    class func fetchExchangeData(_ currencies : [Currency], baseCurrency : Currency, date : Date, success : @escaping (_ result: [CurrencyExchange]?, _ date : Date?) -> Void, failure : @escaping (_ error : Error?) -> Void) {
        let dateString = date.stringValue
        let currenciesString = currencies.map{$0.code}.joined(separator: ",")
        let apiEndpoint = "\(dateString)?symbols=\(currenciesString)&base=\(baseCurrency.code)"
        
        NetowrkingHelper.sharedInstance.get(apiEndpoint, parameters: [:]) {data , error in
            if error != nil {
                failure(error)
            } else if let responseObject = data {
                let rates = responseObject["rates"] as! [String : NSNumber]
                let dateString = responseObject["date"] as! String
                
                var exchangeData : [CurrencyExchange] = []
                if let exchangeDate = Date.dateFromString(dateString) {
                    for currency in currencies {
                        let rate = rates[currency.code];
                        let exchange = CurrencyExchange(currency: currency, baseCurrency: baseCurrency, rate: rate!, date: exchangeDate)
                        exchangeData.append(exchange)
                    }
                    success(exchangeData, exchangeDate)
                } else {
                    success(exchangeData, nil)
                }
            } else {
                failure(nil)
            }
        }
    }
    
    class func fetchRecursiveDataForCurrency(_ currency : Currency, baseCurrency : Currency, days : Int, date : Date, existingData : [CurrencyExchange], success : @escaping (_ result: [CurrencyExchange]?) -> Void, failure : @escaping (_ error : Error?) -> Void) {
        
        var existingExchangeData = existingData
        fetchExchangeData([currency], baseCurrency: baseCurrency, date: date, success: { (result, date) in
            if let resultData = result {
                existingExchangeData += resultData
                
                let remainingDays = days - 1
                if remainingDays == 0 {
                    existingExchangeData = existingExchangeData.reversed()
                    success(existingExchangeData);
                } else {
                    if let previousDay = date?.dateAfterAddingDays(-1) {
                        fetchRecursiveDataForCurrency(currency, baseCurrency: baseCurrency, days: remainingDays, date: previousDay, existingData: existingExchangeData, success: success, failure: failure)
                    } else {
                        //TODO : Custom error code
                        failure(nil)
                    }
                }
            }
            }, failure: failure)
    }
}
