//
//  CurrencyDataStore.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 18/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import Foundation

class CurrencyDataStore: NSObject {
    static let sharedInstance = CurrencyDataStore()
    
    static let defaultCurrencyKey = "DefaultCurrency"
    static let favoriteCurrenciesKey = "FavoriteCurrency"
    
    lazy var userDefaults = UserDefaults.standard
    
    // MARK : Default
    
    func getDefaultCurrency() -> Currency? {
        
        var currency : Currency? = nil
        if let savedCurrency = userDefaults.object(forKey: CurrencyDataStore.defaultCurrencyKey) as? Data {
            currency = NSKeyedUnarchiver.unarchiveObject(with: savedCurrency) as? Currency
        }
        
        return currency
    }
    
    func saveDefaultCurrency(_ currency : Currency) {
        let savedData = NSKeyedArchiver.archivedData(withRootObject: currency)
        userDefaults.set(savedData, forKey: CurrencyDataStore.defaultCurrencyKey)
    }
    
    // MARK: - All Currencies
    
    func getAllCurrencies() -> [Currency] {
        var currencies : [Currency] = []
        if let filePath = Bundle.main.path(forResource: "currencies", ofType: "json") {
            if let data = try? Data(contentsOf: URL(fileURLWithPath: filePath)) {
                do {
                    let jsonCurrency = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [[String]]
                    
                    for currency in jsonCurrency {
                        if let currencyObj = Currency(arrayData: currency) {
                            currencies.append(currencyObj)
                        }
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
        }
        return currencies
    }
    
    // MARK: - Favorite
    
    func getFavoriteCurrencies() -> [Currency] {
        let storedFavoriteCurrencies = userDefaults.object(forKey: CurrencyDataStore.favoriteCurrenciesKey) as? Data
        
        var currencies : [Currency] = []
        if let storedFavoriteCurrencies = storedFavoriteCurrencies {
            let placesArray = NSKeyedUnarchiver.unarchiveObject(with: storedFavoriteCurrencies) as! [Currency]
            currencies = currencies + placesArray
        }
        return currencies
    }
    
    func saveFavoriteCurrencies(_ currencies : [Currency]) {
        if currencies.count > 0 {
            let currenciesData = NSKeyedArchiver.archivedData(withRootObject: currencies)
            userDefaults.set(currenciesData, forKey: CurrencyDataStore.favoriteCurrenciesKey)
        } else {
            userDefaults.removeObject(forKey: CurrencyDataStore.favoriteCurrenciesKey)
        }
    }
}
