//
//  DefaultCurrencySelectionViewController.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 29/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import UIKit

protocol CurrencySelectionControllerDelegate: class {
    func didSelectCurrencies(_ currencies : [Currency], sender: CurrencySelectionViewController)
}

class CurrencySelectionViewController: UIViewController {
    
    enum Mode {
        case defaultCurrencySelection, favoriteCurrencySelection
    }

    
    @IBOutlet var tableView : UITableView!
    
    lazy var currencies : [Currency] = []
    lazy var selectedCurrencies : [Currency] = []
    lazy var mode : Mode = .defaultCurrencySelection
    
    weak var delegate : CurrencySelectionControllerDelegate?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.register(CurrencyListTableViewCell.NibObject(), forCellReuseIdentifier: CurrencyListTableViewCell.identifier)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - IBAction
    
    @IBAction func close(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func done(_ sender: UIButton) {
        dismiss(animated: true) { 
            self.delegate?.didSelectCurrencies(self.selectedCurrencies, sender: self)
        }
    }
    
    func isCurrencySelected(_ currency : Currency) -> Bool {
        return selectedCurrencies.contains(currency)
    }
}

extension CurrencySelectionViewController : UITableViewDataSource {
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CurrencyDataManager.sharedInstance.allCurrencies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CurrencyListTableViewCell.identifier, for: indexPath) as! CurrencyListTableViewCell
        let currency = currencies[indexPath.row]
        let isSelected = isCurrencySelected(currency)
        cell.configureWithCurrency(currency, isFavorite : isSelected)
        return cell
    }
}

extension CurrencySelectionViewController : UITableViewDelegate {
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currency = currencies[indexPath.row]
        
        if mode == .defaultCurrencySelection {
            selectedCurrencies = [currency]
        } else {
            if selectedCurrencies.contains(currency) {
                selectedCurrencies.removeObject(currency)
            } else {
                selectedCurrencies.append(currency)
            }
        }
        
        tableView.reloadData()
    }
}
