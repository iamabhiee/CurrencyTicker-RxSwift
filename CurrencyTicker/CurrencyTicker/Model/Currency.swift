//
//  Currency.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 18/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import Foundation

class Currency : NSObject {
    var name : String!
    var code : String!
    var symbol : String!
    
    override init() {
        
    }
    
    init?(arrayData : [String]?) {
        if let data = arrayData, arrayData?.count == 3 {
            code = data[0]
            name = data[1]
            symbol = data[2]
        } else {
            return nil
        }
    }
    
    override var description : String {
        return "Name : \(name)" +
        "Code : \(code)" +
        "Symbol : \(symbol)"
    }
    
    required init(coder aDecoder: NSCoder) {
        name = aDecoder.decodeObject(forKey: "name") as! String
        code = aDecoder.decodeObject(forKey: "code") as! String
        symbol = aDecoder.decodeObject(forKey: "symbol") as! String
    }
    
    func encodeWithCoder(_ aCoder: NSCoder!) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(code, forKey: "code")
        aCoder.encode(symbol, forKey: "symbol")
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let otherObject = object as? Currency {
            return self.name == otherObject.name && self.code == otherObject.code && self.symbol == otherObject.symbol
        }
        return false
    }
}

