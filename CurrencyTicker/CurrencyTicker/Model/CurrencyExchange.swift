//
//  CurrencyExchangeRate.swift
//  CurrencyTicker
//
//  Created by Abhishek Sheth on 26/08/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

import Foundation

class CurrencyExchange: NSObject {
    var baseCurrency : Currency
    var currency : Currency
    
    var conversationRate : NSNumber
    var previousConversationRate : NSNumber?
    
    var date : Date?
    
    var titleString : String {
        return "\(baseCurrency.code)" +
            " : " +
            "\(currency.code)"
    }
    
    var exchangeRateString : String {
        return String(format: "%.4f", conversationRate.floatValue)
    }
    
    var differenceValue : Float {
        let exchangeRate = self.conversationRate.floatValue
        
        var exchangeRatePreviousValue = Float(0)
        if let prevExchangeRate = self.previousConversationRate {
            exchangeRatePreviousValue = prevExchangeRate.floatValue
        }
        
        let difference = exchangeRate - exchangeRatePreviousValue
        return difference
    }
    
    var differencePercentageValue : Float {
        let exchangeRate = self.conversationRate.floatValue
        
        if exchangeRate > 0.0 {
            let difference = differenceValue
            let percentage = difference / exchangeRate
            return (percentage * 100)
        } else {
            return 0
        }
    }
    
    var isDifferencePostive : Bool {
        return differenceValue > 0.0
    }
    
    var differenceString : String {
        return String(format: "%.4f", differenceValue)
    }
    
    var differencePercentageString : String {
        return String(format: "%.4f", differencePercentageValue) + "%"
    }
    
    init(currency : Currency, baseCurrency : Currency, rate : NSNumber, date : Date) {
        self.currency = currency
        self.baseCurrency = baseCurrency
        self.conversationRate = rate
        self.date = date
    }
}
